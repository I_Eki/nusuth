# Nusuth

> Hot-reload, reactive package tool for developing Chrome-Extension project.

[NPM](https://www.npmjs.com/package/nusuth)
[Bitbucket](https://bitbucket.org/I_Eki/nusuth)
[LICENSE](https://bitbucket.org/I_Eki/nusuth/src/master/LICENSE)

## Install

```bash
# install dependencies
npm i nusuth -D

# or use yarn
yarn add nusuth -D
```

## Usage

_Todo_

## Options

_Todo_

## License

[MIT](https://bitbucket.org/I_Eki/nusuth/src/master/LICENSE).
