import { Module } from '../nusuth/bundle';
import { SpecialModules } from '../types';
import type { Plugin } from 'rollup';

const PLUGIN_NAME = 'rollup-plugin-nusuth-background';

export interface PluginOptions {
  module: Module;
}

export function background({ module }: PluginOptions): Plugin {
  let input = '';

  return {
    name: PLUGIN_NAME,
    buildStart(options) {
      input = (options.input as string[])[0];
    },
    async transform(code: string, id: string) {
      if (module.name === SpecialModules.BACKGROUND && input === id) {
        return code.replace(
          /[\r\n]import[^\n\r]+\.(css|less)['"];?[\n\r]/,
          '\n',
        );
      }

      return code;
    },
  };
}
