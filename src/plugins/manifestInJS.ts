import type { Plugin } from 'rollup';
import jsYaml from 'js-yaml';
import { existsSync } from 'fs';
import { Module } from '../nusuth/bundle';
import {
  FullPermission,
  Manifest,
  NusuthConfig,
  OptionalPermission,
  Permission,
  SpecialModules,
} from '../types';
import { deepMerge, resolveChromePath } from 'src/utils/util';

const PLUGIN_NAME = 'rollup-plugin-manifest-in-js';
const yamlRe = /^```\s+([\S\s]+)\s+```\s+/;

export interface PluginOptions {
  module: Module;
  manifest: Manifest;
  config: NusuthConfig;
}

interface ResolveOptions {
  module: Module;
  manifest: Manifest;
  yamlObj: any;
  hasCSS: boolean;
  scriptPath: string;
  cssPath: string;
  needs: FullPermission[];
}

/** 处理模块所需权限 */
export function resolvePermission({
  manifest,
  yamlObj,
  needs,
}: ResolveOptions) {
  if (!manifest.optional_permissions) {
    manifest.optional_permissions = [];
  }

  if (yamlObj.permissions) {
    if (typeof yamlObj.permissions === 'string') {
      manifest.optional_permissions.push(yamlObj.permissions);
    } else if (yamlObj.permissions instanceof Array) {
      manifest.optional_permissions.push(...yamlObj.permissions);
    }
    delete yamlObj.permissions;
  }

  needs.forEach((item) => {
    if ((<any>Object).values(Permission).includes(item)) {
      if (!manifest.permissions!.includes(item)) {
        manifest.permissions!.push(item);
      }
    } else if ((<any>Object).values(OptionalPermission).includes(item)) {
      if (!manifest.optional_permissions!.includes(<OptionalPermission>item)) {
        manifest.optional_permissions!.push(<OptionalPermission>item);
      }
    }
  });
}

/** 解析background配置并完成对应操作 */
export function resolveBackground({
  manifest,
  yamlObj,
  scriptPath,
  needs,
}: ResolveOptions) {
  if (!yamlObj.scripts) yamlObj.scripts = [];
  yamlObj.scripts.push(scriptPath);
  manifest.background = deepMerge(
    {
      persistent: yamlObj.persistent || false,
      scripts: yamlObj.scripts,
    },
    manifest.background || {},
  );

  needs.push(OptionalPermission.BACKGROUND);
}

/** 解析content_scripts配置并完成对应操作 */
export function resolveContentScripts({
  manifest,
  yamlObj,
  hasCSS,
  scriptPath,
  cssPath,
  needs,
}: ResolveOptions) {
  if (!manifest.content_scripts) {
    manifest.content_scripts = [];
  }

  if (hasCSS) {
    yamlObj.css = [cssPath];
  }

  yamlObj.js = [scriptPath];
  manifest.content_scripts.push(yamlObj);

  needs.push(OptionalPermission.TABS);
}

export function manifestInJS({
  module,
  config,
  manifest,
}: PluginOptions): Plugin {
  let yamlObj: any = null;
  let input = '';

  return {
    name: PLUGIN_NAME,
    buildStart(options) {
      input = (options.input as string[])[0];
    },
    async transform(code: string, id: string) {
      if (input !== id) return code;

      if (yamlRe.test(code)) {
        const yaml = code.match(yamlRe)![1];
        yamlObj = jsYaml.load(yaml);
      }

      yamlObj = yamlObj || {};
      return code.replace(yamlRe, '');
    },
    writeBundle(outputOptions) {
      const file = outputOptions.file!;
      const output = config.output!;
      const cssOutput = file && file.replace(/\.js$/, '.css');
      const hasCSS = cssOutput ? existsSync(cssOutput) : false;
      const resolveOptions: ResolveOptions = {
        module,
        manifest,
        yamlObj,
        hasCSS,
        scriptPath: resolveChromePath(file && file.replace(output, '')),
        cssPath: resolveChromePath(hasCSS ? cssOutput.replace(output, '') : ''),
        needs: [],
      };

      switch (module.name) {
        case SpecialModules.BACKGROUND:
          resolveBackground(resolveOptions);
          break;
        case SpecialModules.OPTION:
          break;
        case SpecialModules.DEVTOOLS:
          break;
        case SpecialModules.POPUP:
          break;
        case SpecialModules.OVERRIDES:
          break;
        default:
          // content_scripts
          resolveContentScripts(resolveOptions);
          break;
      }

      resolvePermission(resolveOptions);
    },
  };
}
