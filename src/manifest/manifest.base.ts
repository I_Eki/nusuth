import { Manifest } from '../types';

export default function (): Manifest {
  return {
    manifest_version: 2,
    name: 'my-nusuth-crx',
    version: '0.0.1',
    description: 'A chrome extension build by Nusuth',
    optional_permissions: [],
    permissions: [],
  };
}
