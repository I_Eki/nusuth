import { Manifest } from '../types';

export default function (): Manifest {
  return {
    background: {
      scripts: ['/js/hot-reload.js'],
    },
  };
}
