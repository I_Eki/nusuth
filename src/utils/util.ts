import {
  readdirSync,
  rmdirSync,
  rmSync,
  statSync,
  copyFileSync,
  existsSync,
  mkdirSync,
} from 'fs';
import { resolve } from 'path';

/** 深度合并对象属性 */
export function deepMerge(src: any, base: any) {
  const target = JSON.parse(JSON.stringify(base));

  Object.getOwnPropertyNames(src).forEach((key) => {
    if (!base[key] || typeof base[key] !== typeof src[key]) {
      target[key] = src[key];
    } else if (base[key] instanceof Array && src[key] instanceof Array) {
      target[key] = [...new Set([...base[key], ...src[key]])];
    } else if (typeof src[key] === 'object' && typeof base[key] === 'object') {
      target[key] = deepMerge(src[key], base[key]);
    }
  });

  return target;
}

// 生成一个20位的hash字符串
export function getUUID() {
  return 'xxyyxxyyxxyyxxyyxxyy'.replace(/[xy]/g, function (c) {
    const r = (Math.random() * 16) | 0;
    const v = c == 'x' ? r : (r & 0x3) | 0x8;

    return v.toString(16);
  });
}

/**
 * 删除目录及文件
 * @param {string} path 文件夹路径
 * @param {boolean} isRemoveSelf 是否删除自身，为否则只删除子目录及目录下文件
 */
export function removeDir(path: string, isRemoveSelf = false) {
  readdirSync(path).forEach((p) => {
    p = resolve(path, p);
    const stat = statSync(p);
    if (stat.isDirectory()) {
      removeDir(p, true);
    } else {
      rmSync(p);
    }
  });
  if (isRemoveSelf) rmdirSync(path);
}

/**
 * 复制目录及文件
 * @param {string} path 文件夹路径
 */
export function copyDir(src: string, dest: string, isReplace: boolean = false) {
  readdirSync(src).forEach((p) => {
    const q = resolve(dest, p);
    p = resolve(src, p);
    const stat = statSync(p);
    if (stat.isDirectory()) {
      if (!existsSync(q)) mkdirSync(q);
      copyDir(p, q);
    } else {
      if (isReplace || !existsSync(q)) copyFileSync(p, q);
    }
  });
}

/**
 *获取当前是否为开发环境
 * @returns {boolean} 是否为开发环境
 */
export function isDev() {
  return process.env.NODE_ENV !== 'production';
}

/** 转换路径字符串为chrome extension的路径格式 */
export function resolveChromePath(path: string) {
  return path.replace(/\\/g, '/');
}
