export enum SpecialModules {
  BACKGROUND = 'background',
  OPTION = 'options_ui',
  POPUP = 'browser_action',
  DEVTOOLS = 'devtools',
  OVERRIDES = 'overrides',
}

export interface NusuthConfig {
  root?: string;
  output?: string;
  assetsDirName?: string;
}
export interface BackgroundModule {
  persistent?: boolean;
  scripts: string[];
}

export interface Icons {}

export interface IconActions {
  default_icon: string;
  default_title: string;
  default_popup: string;
}

export type URLOverrides = {
  [key in 'bookmarks' | 'history' | 'newtab']?: string;
};

export interface ContentScript {
  matches: string[];
  css?: string[];
  js: string[];
}

export enum Permission {
  DEBUGGER = 'debugger',
  DECLARATIVE_NET_REQUEST = 'declarativeNetRequest',
  DEVTOOLS = 'devtools',
  EXPERIMENTAL = 'experimental',
  GEOLOCATION = 'geolocation',
  MDNS = 'mdns',
  PROXY = 'proxy',
  TTS = 'tts',
  TTS_ENGINE = 'ttsEngine',
  WALLPAPER = 'wallpaper',
}

export enum OptionalPermission {
  ACTIVE_TAB = 'activeTab',
  ALARMS = 'alarms',
  BACKGROUND = 'background',
  BOOKMARKS = 'bookmarks',
  BROWSING_DATA = 'browsingData',
  CERTIFICATE_PROVIDER = 'certificateProvider',
  CLIPBOARD_READ = 'clipboardRead',
  CLIPBOARD_WRITE = 'clipboardWrite',
  CONTENT_SETTINGS = 'contentSettings',
  CONTEXT_MENUS = 'contextMenus',
  COOKIES = 'cookies',
  DEBUGGER = 'debugger',
  DECLARATIVE_CONTENT = 'declarativeContent',
  DECLARATIVE_NET_REQUEST = 'declarativeNetRequest',
  DECLARATIVE_NEW_REQUEST_FEEDBACK = 'declarativeNetRequestFeedback',
  DECLARATIVE_WEB_REQUEST = 'declarativeWebRequest',
  DESKTOPCAPTURE = 'desktopCapture',
  DOCUMENT_SCAN = 'documentScan',
  DOWNLOADS = 'downloads',
  'ENTERPRISE.DEVICE_ATTRIBUTES' = 'enterprise.deviceAttributes',
  'ENTERPRISE.HARDWARE_PLATFORM' = 'enterprise.hardwarePlatform',
  'ENTERPRISE.NETWORKING_ATTRIBUTES' = 'enterprise.networkingAttributes',
  'ENTERPRISE.PLATFORM_KEYS' = 'enterprise.platformKeys',
  EXPERIMENTAL = 'experimental',
  FILE_BROWSER_HANDLER = 'fileBrowserHandler',
  FILE_SYSTEM_PROVIDER = 'fileSystemProvider',
  FONT_SETTINGS = 'fontSettings',
  GCM = 'gcm',
  GEOLOCATION = 'geolocation',
  HISTORY = 'history',
  IDENTITY = 'identity',
  IDLE = 'idle',
  LOGIN_STATE = 'loginState',
  MANAGEMENT = 'management',
  NATIVE_MESSAGING = 'nativeMessaging',
  NOTIFICATIONS = 'notifications',
  PAGE_CAPTURE = 'pageCapture',
  PLATFORM_KEYS = 'platformKeys',
  POWER = 'power',
  PRINTER_PROVIDER = 'printerProvider',
  PRINTING = 'printing',
  PRINTING_METRICS = 'printingMetrics',
  PRIVACY = 'privacy',
  PROCESSES = 'processes',
  PROXY = 'proxy',
  SCRIPTING = 'scripting',
  SEARCH = 'search',
  SESSIONS = 'sessions',
  SIGNED_IN_DEVICES = 'signedInDevices',
  STORAGE = 'storage',
  'SYSTEM.CPU' = 'system.cpu',
  'SYSTEM.DISPLAY' = 'system.display',
  'SYSTEM.MEMORY' = 'system.memory',
  'SYSTEM.STORAGE' = 'system.storage',
  TAB_CAPTURE = 'tabCapture',
  TAB_GROUPS = 'tabGroups',
  TABS = 'tabs',
  TOP_SITES = 'topSites',
  TTS = 'tts',
  TTS_ENGINE = 'ttsEngine',
  UNLIMITED_STORAGE = 'unlimitedStorage',
  VPN_PROVIDER = 'vpnProvider',
  WALLPAPER = 'wallpaper',
  WEB_NAVIGATION = 'webNavigation',
  WEB_REQUEST = 'webRequest',
  WEB_REQUEST_BLOCKING = 'webRequestBlocking',
}

export type FullPermission = Permission | OptionalPermission;

export interface Manifest {
  manifest_version?: 2;
  name?: string;
  version?: string;

  // Recommended
  default_locale?: string;
  description?: string;
  icons?: Icons;

  // Pick one (or none)
  browser_action?: IconActions;
  page_action?: IconActions;

  // Optional
  author?: string;
  // chrome_settings_overrides: {...};
  chrome_url_overrides?: URLOverrides;
  // commands: {...};
  // content_capabilities: ...;
  background?: BackgroundModule;
  content_scripts?: ContentScript[];
  // content_security_policy: policyString;
  // converted_from_user_script: ...;
  // cross_origin_embedder_policy: {value: require-corp};
  // cross_origin_opener_policy: {value: same-origin};
  // current_locale: ...;
  // declarative_net_request: ...;
  // devtools_page: devtools.html;
  // differential_fingerprint: ...;
  // event_rules: [{...}];
  // externally_connectable: {
  //   matches: [*://*.example.com/*]
  // };
  // file_browser_handlers: [...];
  // file_system_provider_capabilities: {
  //   configurable: true;
  //   multiple_mounts: true;
  //   source: network
  // };
  // homepage_url: http://path/to/homepage;
  // host_permissions: ...;
  // import: [{id: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa}];
  // incognito: spanning, split, or not_allowed;
  // input_components: ...;
  // key: publicKey;
  // minimum_chrome_version: versionString;
  // nacl_modules: [...];
  // natively_connectable: ...;
  // oauth2: ...;
  // offline_enabled: true;
  // omnibox: {
  //   keyword: aString
  // };
  optional_permissions?: OptionalPermission[];
  // options_page: options.html;
  // options_ui: {
  //   chrome_style: true;
  //   page: options.html
  // };
  permissions?: FullPermission[];
  // platforms: ...;
  // replacement_web_app: ...;
  // requirements: {...};
  // sandbox: [...];
  // short_name: Short Name;
  // storage: {
  //   managed_schema: schema.json
  // };
  // system_indicator: ...;
  // tts_engine: {...};
  // update_url: http://path/to/updateInfo.xml;
  // version_name: ...;
  // web_accessible_resources: [...]
}
