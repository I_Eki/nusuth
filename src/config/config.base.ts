import { NusuthConfig } from '../types';
import { resolve } from 'path';

const root = process.cwd();

export default function (): NusuthConfig {
  return {
    root: resolve(root, 'src'),
    output: resolve(root, 'dist'),
    assetsDirName: 'assets',
  };
}
