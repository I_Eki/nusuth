import BaseConfig from './config.base';
import DevConfig from './config.dev';
import ProdConfig from './config.prod';
import { deepMerge, isDev } from '../utils/util';

export interface NusuthConfig {
  root: string;
  output: string;
}

export default function (): NusuthConfig {
  let config = BaseConfig();

  if (isDev()) {
    const devConfig = DevConfig();

    config = deepMerge(devConfig, config);
  } else {
    const prodConfig = ProdConfig();

    config = deepMerge(prodConfig, config);
  }

  return config as NusuthConfig;
}
