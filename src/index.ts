require('module-alias/register');
import './nusuth/common';
import './nusuth/bundle';
import './nusuth/manifest';
import Nusuth from './nusuth';
import { defineConfig } from './nusuth/config';

export { defineConfig };

export default Nusuth;
