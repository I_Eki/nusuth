import { Module } from './bundle';
import Nusuth, { INusuth } from './index';
import { isDev } from '../utils/util';
import { writeFileSync } from 'fs';
import { resolve } from 'path';
import { Manifest } from '../types';

export interface INusuthCommon {
  isDev: boolean;
  manifest: Manifest;
  modules: Module[];

  checkSituation: () => void;
  dev: () => void;
}

Nusuth.prototype.checkSituation = function () {};

Nusuth.prototype.dev = async function (this: INusuth) {
  this.readConfig();
  this.initOutputDir();
  this.isDev = isDev();

  this.manifest = this.readManifest();
  this.modules = this.getModules();

  const promises = this.modules.map((m) => this.bundleModule(m));

  await Promise.all(promises);

  writeFileSync(
    resolve(this.config.output!, 'manifest.json'),
    JSON.stringify(this.manifest, null, this.isDev ? 2 : 0),
  );

  this.copyStaticFiles();
};

export default Nusuth;
