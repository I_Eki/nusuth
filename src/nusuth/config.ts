import { existsSync } from 'fs';
import Nusuth, { INusuth } from './index';
import { deepMerge } from '../utils/util';
import { resolve } from 'path';
import baseConfig, { NusuthConfig } from '../config';

const configName = 'nusuth.config.js';

export interface INusuthConfig {
  config: NusuthConfig;

  readConfig: () => void;
}

export const defineConfig = (config: NusuthConfig) => config;

/**
 * 读取Nusuth项目配置
 * @returns
 */
Nusuth.prototype.readConfig = function (this: INusuth) {
  const configPath = resolve(this.projRoot, configName);
  const baseConfigObj = baseConfig();
  if (!existsSync(configPath)) return baseConfigObj;

  let projConfig = require(configPath);

  if (typeof projConfig === 'function') {
    projConfig = projConfig();
  } else if (projConfig.toString() !== '[object Object]') {
    throw new Error(
      'Bad config file! Need return an object or a function that will return an object.',
    );
  }

  this.config = deepMerge(projConfig, baseConfigObj);
  return this.config;
};

export default Nusuth;
