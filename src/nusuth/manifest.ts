import { existsSync } from 'fs';
import { resolve } from 'path';
import { deepMerge } from '../utils/util';
import Nusuth from './index';
import baseManifest from '../manifest';
import { Manifest } from '../types';

export interface INusuthManifest {
  readManifest: () => Manifest;
}

/**
 * 获取扩展的主配置对象
 * @returns {Object} 扩展的manifest配置对象
 */
Nusuth.prototype.readManifest = function () {
  const mfPath = resolve(this.config.root, 'manifest.json');
  let baseManifestObj = baseManifest();

  if (!existsSync(mfPath)) {
    const mf = require(mfPath);
    this.manifest = deepMerge(mf, baseManifestObj);
  } else {
    this.manifest = baseManifestObj;
  }

  return this.manifest;
};

export default Nusuth;
