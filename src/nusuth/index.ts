import { INusuthCommon } from './common';
import { INusuthBundle } from './bundle';
import { INusuthConfig } from './config';
import { INusuthManifest } from './manifest';
import { resolve } from 'path';

export interface INusuth
  extends INusuthCommon,
    INusuthBundle,
    INusuthConfig,
    INusuthManifest {
  projRoot: string;
  root: string;
}

export default function Nusuth(this: INusuth) {
  this.projRoot = process.cwd();
  this.root = resolve(this.projRoot, 'src');
}
