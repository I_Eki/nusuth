import { existsSync, mkdirSync, readdirSync, statSync } from 'fs';
import { resolve } from 'path';
import { copyDir, getUUID, removeDir } from '../utils/util';
import { rollup, RollupOptions, OutputOptions } from 'rollup';
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
// import ts from 'rollup-plugin-typescript2';
import { terser } from 'rollup-plugin-terser';
import postcss from 'rollup-plugin-postcss';
import { manifestInJS } from '../plugins/manifestInJS';
import { background } from '../plugins/background';
import Nusuth from './index';

export interface Module {
  name: string;
  path: string;
}

export interface ModuleOptions {
  type: string;
  inputOptions: RollupOptions;
  outputOptions: OutputOptions;
}

export interface INusuthBundle {
  getModules: () => Module[];
  initOutputDir: () => void;
  bundleModule: (module: Module) => void;
  copyStaticFiles: () => void;
}

/**
 * 获取项目所有模块路径列表
 */
Nusuth.prototype.getModules = function () {
  const modulePath = resolve(this.config.root, 'modules');
  if (!existsSync(modulePath)) return [];

  const childs = readdirSync(modulePath);
  const modules: Module[] = [];

  childs.forEach((child) => {
    const childPath = resolve(modulePath, child);
    const childStat = statSync(childPath);

    if (childStat.isFile()) return;
    modules.push({
      name: child,
      path: childPath,
    });
  });

  return modules;
};

/**
 * 初始化输出目录状态
 */
Nusuth.prototype.initOutputDir = function () {
  if (!existsSync(this.config.output)) {
    mkdirSync(this.config.output);
  }

  const assetsPath = resolve(this.config.output, this.config.assetsDirName);
  if (!existsSync(assetsPath)) {
    mkdirSync(assetsPath);
  } else {
    removeDir(assetsPath);
  }
};

Nusuth.prototype.bundleModule = async function (module: Module) {
  const uuid = getUUID();

  const plugins = [
    nodeResolve({ mainFields: ['jsnext'] }),
    commonjs(),
    postcss({
      extensions: ['css', 'less'],
      inject: false,
      extract: true,
      autoModules: false,
      minimize: true,
    }),
    manifestInJS({
      module,
      config: this.config,
      manifest: this.manifest,
    }),
    background({
      module,
    }),
  ];

  // if (/\.ts+(|x)$/.test(module.path)) {
  //   plugins.push(
  //     ts({
  //       tsconfig: resolve(__dirname, './tsconfig.json'),
  //     }),
  //   );
  // }

  this.isDev ||
    plugins.unshift(
      terser({
        compress: {
          negate_iife: false,
        },
      }),
    );

  const inputOptions = {
    input: resolve(module.path, 'index.js'),
    plugins,
  };

  const filePath = resolve(
    this.config.output,
    this.config.assetsDirName,
    `${module.name}.${uuid}.js`,
  );
  const outputOptions: OutputOptions = {
    exports: 'none',
    file: filePath,
    format: 'iife',
  };

  const bundle = await rollup(inputOptions);

  await bundle.generate(outputOptions);
  await bundle.write(outputOptions);

  await bundle.close();

  return filePath;
};

Nusuth.prototype.copyStaticFiles = function () {
  const publicPaths = [resolve(this.root, '../public')];

  if (this.isDev) {
    publicPaths.push(resolve(__dirname, '../public'));
  }

  publicPaths.forEach((path) => {
    if (!existsSync(path)) return;

    copyDir(path, this.config.output);
  });
};

export default Nusuth;
