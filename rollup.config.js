import json from 'rollup-plugin-json';
import ts from 'rollup-plugin-typescript2';
import commonjs from 'rollup-plugin-commonjs';
import packageJSON from './package.json';
import { resolve } from 'path';

export default {
  input: './src/index.ts',
  output: [
    {
      name: 'Nusuth',
      exports: 'named',
      file: packageJSON.main,
      format: 'umd'
    },
    {
      exports: 'named',
      file: packageJSON.module,
      format: 'es'
    }
  ],
  watch: {
    include: 'src/**/*'
  },
  plugins: [
    json(),
    commonjs(),
    ts({
      tsconfig: resolve(__dirname, './tsconfig.json'),
      exclude: ['*.js+(|x)', '**/*.js+(|x)'],
    }),
  ]
}
